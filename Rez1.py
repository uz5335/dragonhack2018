from keras.models import Sequential
from keras.layers import Dense
from keras.models import model_from_json
import numpy as np
import csv
import os
import sys

def napovej(q,w,e,r,t,z,u,i,o,p,a,s):
    set = [q,w,e,r,t,z,u,i,o,p,a,s]
    data = np.array([set])
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("model.h5")


    rez = loaded_model.predict_classes(data)[0]
    toprez =0
    topplus = 0
    topoperand = ""
    topatribut = ""


    for procenti in range (100):

        if(toprez >rez):
            print (rez, toprez, topplus, topoperand, topatribut)
            exit(0)
        for atribut in range (12):

            set2 = set
            set2[atribut] = set2[atribut]+ set2[atribut] * procenti/100
            rez2 = loaded_model.predict_classes(np.array([set2]))[0]

            if (rez2 > rez):
                if (rez2 > toprez):
                    toprez = rez2
                    topplus = procenti
                    topoperand = 1
                    topatribut = atribut
            set2 = set
            set2[atribut] = set2[atribut] - procenti / 10
            rez2 = loaded_model.predict_classes(np.array([set2]))[0]

            if (rez2 > rez):
                if (rez2 > toprez):
                    toprez = rez2
                    topplus = procenti
                    topoperand = 0
                    topatribut = atribut


    print(-1, -1, -1, -1, -1)

napovej(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5],sys.argv[6],sys.argv[7],sys.argv[8],sys.argv[9],sys.argv[10],sys.argv[11],sys.argv[12])