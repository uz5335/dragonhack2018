window.addEventListener("load", function(){
  
    var myConfig = {
        "type":"radar",
        "type": "radar",
  "legend":{
    "align":"center",
    "vertical-align":"bottom"
  },
        "plot":{
            "aspect":"area"
        },
          "scale-v": {
            "values": "0:100:25",
            "labels": ["", "", "", "", ""],
            "ref-line": {
              "line-color": "none"
            },
            "guide": {
              "line-style": "solid"
            }
          },
            "scale-k": {
    "values": ["Fixed acidity","Volatile acidity", "Citric acid","Residual sugar", "Chlorides","Free sulfur dioxide", "Total sulfur dioxide","Density", "pH","Sulphates", "Alcohol"],
    "format": "",
    "aspect": "circle", //To set the chart shape to circular.
    "guide": {
      "line-style": "solid"
    }
  },
             "series":[
            {
                "values":[69,53,27,100,84,45,33,27,52,78,92],
                "text":"Average values"
            },
            {
                "values": [
         Number($("#fixedAcidity").val()), Number($("#volatileAcidity").val()),
        Number($("#citricAcid").val()),Number($("#residualSugar").val()),Number($("#chlorides").val()),
        Number($("#freeSulfurDioxide").val()),Number( $("#totalSulfurDioxide").val()),Number( $("#density").val()),
       Number($("#pH").val()), Number($("#sulphates").val()), Number($("#alcohol").val())
    ],
                "text":"Your values"
            }
            ,
            {
                "values":[60,64,21,99,60,70,67,60,32,80,45],
                "text":"Best values"
            }
        ]
    };
     
    zingchart.render({ 
    	id : 'myChart', 
    	data : myConfig, 
    	height: '100%', 
    	width: '100%' 
    });



var ocena; 
if($("#density")>5){
    ocena = 7;
}
else ocena = 6;

    zingchart.THEME="classic";
    var myConfig = {
        "background-color": "#f5f7ea",
        "graphset": [
            {
                "type": "null",
                "x": "2%",
                "y": "3%",
                "height": "25%",
                "width": "31%",
                "border-width": "1px",
                "border-color": "#384653",
                "border-radius": 4,
                "background-color": "#fbfcf7",
                "title": {
                    "text": "All instances",
                    "background-color": "none",
                    "font-color": "#384653",
                    "font-size": "12px",
                    "text-align": "center",
                    "height": "70px"
                },
                "subtitle": {
                    "text": "6920",
                    "font-color": "#dd655f",
                    "font-size": "24px",
                    "bold": true,
                    "text-align": "center",
                    "height": "40px",
                    "padding-top": "25%"
                }
            },
            {
                "type": "null",
                "x": "34.6%",
                "y": "3%",
                "height": "25%",
                "width": "31%",
                "border-width": "1px",
                "border-color": "#384653",
                "border-radius": 4,
                "background-color": "#fbfcf7",
                "title": {
                    "text": "Average result",
                    "background-color": "none",
                    "font-color": "#384653",
                    "font-size": "12px",
                    "text-align": "center",
                    "height": "70px"
                },
                "subtitle": {
                    "text": "6.7",
                    "font-color": "#4cc2bb",
                    "font-size": "24px",
                    "bold": true,
                    "text-align": "center",
                    "height": "40px",
                    "padding-top": "25%"
                }
            },
            {
                "type": "null",
                "x": "67%",
                "y": "3%",
                "height": "25%",
                "width": "31%",
                "border-width": "1px",
                "border-color": "#384653",
                "border-radius": 4,
                "background-color": "#fbfcf7",
                "title": {
                    "text": "Your result",
                    "font-color": "#104053",
                    "font-size": "12px",
                    "background-color": "none",
                    "text-align": "center",
                    "height": "70px"
                },
                "subtitle": {
                    "text": ocena,
                    "font-color": "#4f5963",
                    "font-size": "24px",
                    "bold": true,
                    "text-align": "center",
                    "height": "40px",
                    "padding-top": "25%"
                }
            },
            {
                "type": "bar",
                "x": "2%",
                "y": "30%",
                "height": "68%",
                "width": "96%",
                "border-width": "1px",
                "border-color": "#384653",
                "border-radius": 4,
                "background-color": "#fbfcf7",
                "legend": {
                    "margin":"auto auto 10% auto",
                    "toggle-action": "remove",
                    "shadow": false,
                    "border-radius": 4,
                    "background-color": "#FFFFFF",
                    "border-color": "#FFFFFF",
                    "layout": "float"
                },
                "plotarea": {
                    "margin": "45 40 90 60"
                },
                "scale-x": {
                    "values": [
                        
                        "fixed acidity",
                        "volatile acidity",
                        "citric acid",
                        "residual sugar",
                        "chlorides",
                        "free sulfur dioxide",
                        "total sulfur dioxide",
                        "density",
                        "pH",
                        "sulphates",
                        "alcohol"
                    ],
                    "line-color": "#b0aaab",
                    "line-width": 1,
                    "guide": {
                        "visible": false
                    },
                    "item": {
                        "font-color": "#384653"
                    },
                    "tick": {
                        "visible": false
                    }
                },
                "scale-y": {
                    "values": "0:80:10",
                    "line-color": "#FFFFFF",
                    "line-width": 1,
                    "guide": {
                        "visible": true,
                        "line-style": "solid"
                    },
                    "item": {
                        "padding-right": "5%",
                        "font-color": "#384653"
                    },
                    "tick": {
                        "visible": false
                    }
                },
                "series": [
                    
                    {
                        "values": [69,53,27,100,84,45,33,27,52,78,92],
                        "background-color": "#4c707e",
                        "text": "Average values",
                        "legend-marker": {
                            "border-color": "blue"
                        }
                    },
                    {
                        "values": [76,60,11,100,99,60,24,45,60,50,90],
                        "background-color": "red",
                        "text": "Your values",
                        "legend-marker": {
                            "border-color": "#dd655f"
                        }
                    },
                    {
                        "values": [60,64,21,99,60,70,67,60,32,80,45],
                        "background-color": "green",
                        "text": "Best values",
                        "legend-marker": {
                            "border-color": "#88a0a9"
                        }
                    }
                ],
                "tooltip": {
                    "text": "%v %k in %t",
                    "shadow": false,
                    "border-radius": 4
                }
            }
        ]
    };
     
    zingchart.render({ 
    	id : 'myChart2', 
    	data : myConfig, 
    	height: "50%", 
    	width: '100%' 
    });



    zingchart.THEME="classic";
    var myConfig = {
        "background-color":"white",
        "type":"line",
        "title":{
            "text":"Yearly Outbreaks by Genus",
            "color":"#333",
            "background-color":"white",
            "width":"60%",
            "text-align":"left",
        },
        "subtitle":{
            "text":"Toggle a legend item to remove the series and adjust the scale.",
            "text-align":"left",
            "width":"60%"
        },
    	"legend":{
            "layout":"x1",
            "margin-top":"5%",
            "border-width":"0",
            "shadow":false,
            "marker":{
                "cursor":"hand",
                "border-width":"0"
            },
            "background-color":"white",
            "item":{
                "cursor":"hand"
            },
            "toggle-action":"remove"
        },
    	"scaleX":{
            "values":"1998:2012:1",
            "max-items":8
    	},
    	"scaleY":{
            "line-color":"#333"
    	},
        "tooltip":{
            "text":"%t: %v outbreaks in %k"
        },
    	"plot":{
            "line-width":3,
            "marker":{
                "size":2
            },
            "selection-mode":"multiple",
            "background-mode":"graph",
            "selected-state":{
                "line-width":4
            },
            "background-state":{
                "line-color":"#eee",
                "marker":{
                    "background-color":"none"
                }
            }
    	},
        "plotarea":{
            "margin":"15% 25% 10% 7%"
        },
    	"series":[
            {
                "values":[783,672,621,466,427,315,382,299,363,363,350,213,261,287,243],
                "text":"Undeclared",
                "line-color":"#a6cee3",
                "marker":{
                    "background-color":"#a6cee3",
                    "border-color":"#a6cee3"
                }
            },
            {
                "values":[148,137,149,134,132,136,141,115,120,146,117,118,132,114,116],
                "text":"Salmonella",
                "line-color":"#1f78b4",
                "marker":{
                    "background-color":"#1f78b4",
                    "border-color":"#1f78b4"
                }
            },
            {
                "values":[73,199,276,305,367,285,496,283,503,321,358,198,303,224,288],
                "text":"Norovirus",
                "line-color":"#b2df8a",
                "marker":{
                    "background-color":"#b2df8a",
                    "border-color":"#b2df8a"
                }
            },
            {
                "values":[19,12,6,6,8,9,9,6,8,1,2,2,6,8,12],
                "text":"Vibrio",
                "line-color":"#33a02c",
                "marker":{
                    "background-color":"#33a02c",
                    "border-color":"#33a02c"
                }
            },
            {
                "values":[32,31,35,24,33,26,21,23,30,44,36,36,30,28,30],
                "text":"E.coli",
                "line-color":"#fb9a99",
                "marker":{
                    "background-color":"#fb9a99",
                    "border-color":"#fb9a99"
                }
            },
            {
                "values":[12,12,12,6,7,6,2,6,5,4,1,2,3,1,0],
                "text":"Hepatitis",
                "line-color":"#e31a1c",
                "marker":{
                    "background-color":"#e31a1c",
                    "border-color":"#e31a1c"
                }
            },
            {
                "values":[39,58,63,59,64,50,39,34,29,21,14,13,10,12,8],
                "text":"Staphylococcus",
                "line-color":"#fdbf6f",
                "marker":{
                    "background-color":"#fdbf6f",
                    "border-color":"#fdbf6f"
                }
            },
            {
                "values":[56,53,60,61,65,52,58,42,39,50,44,30,34,23,32],
                "text":"Clostridium",
                "line-color":"#ff7f00",
                "marker":{
                    "background-color":"#ff7f00",
                    "border-color":"#ff7f00"
                }
            },
            {
                "values":[15,6,15,20,22,22,15,25,27,30,26,16,28,33,38],
                "text":"Campylobacter",
                "line-color":"#cab2d6",
                "marker":{
                    "background-color":"#cab2d6",
                    "border-color":"#cab2d6"
                }
            },
            {
                "values":[19,15,13,16,12,14,11,8,10,11,6,4,5,4,2],
                "text":"Shigella",
                "line-color":"#ffff99",
                "marker":{
                    "background-color":"#ffff99",
                    "border-color":"#ffff99"
                }
            },
            {
                "values":[33,30,35,26,27,38,29,25,30,18,9,5,10,10,15],
                "text":"Scombroid",
                "line-color":"#6a3d9a",
                "marker":{
                    "background-color":"#6a3d9a",
                    "border-color":"#6a3d9a"
                }
            },
            {
                "values":[1,1,0,3,3,1,2,0,0,0,0,0,0,1,2],
                "text":"Yersinia",
                "line-color":"#b15928",
                "marker":{
                    "background-color":"#b15928",
                    "border-color":"#b15928"
                }
            },
            {
                "values":[32,32,56,62,82,65,57,40,33,41,26,12,15,10,3],
                "text":"Bacillus"
            },
            {
                "values":[2,5,2,2,1,2,0,4,4,1,3,2,5,5,5],
                "text":"Listeria"
            },
            {
                "values":[1,0,1,0,0,0,0,0,0,0,0,1,1,0,0],
                "text":"Pesticides"
            },
            {
                "values":[1,2,2,2,3,0,3,5,3,0,3,1,0,2,0],
                "text":"Cyclospora"
            },
            {
                "values":[18,15,19,26,23,18,10,10,10,14,15,10,5,15,8],
                "text":"Ciguatoxin"
            },
            {
                "values":[1,0,1,0,2,2,1,0,4,4,2,0,0,4,0],
                "text":"Cryptosporidium"
            },
            {
                "values":[1,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                "text":"Streptococcus"
            },
            {
                "values":[27,53,28,11,32,38,35,33,33,13,6,4,2,17,13],
                "text":"Other"
            }
    	]
    };
     
    zingchart.render({ 
    	id : 'myChart3', 
    	data : myConfig, 
    	height: 500, 
    	width: 725 
    });

var myConfig2 = {
        "type":"bar3d",
        "background-color":"#fff",
        "3d-aspect":{
            "true3d":0,
            "y-angle":10,
            "depth":30
        },
        "title":{
            "text":"Diference with best attributes",
            "height":"40px",
            "font-weight":"normal",
            "text-color":"#ffffff"
        },
        "legend":{
            "layout":"float",
            "background-color":"none",
            "border-color":"none",
            "item":{
                "font-color":"#333"
            },
            "x":"37%",
            "y":"10%",
            "width":"90%",
            "shadow":0
        },
        "plotarea":{
            "margin":"95px 35px 50px 70px",
            "background-color":"#fff",
            "alpha":0.3
        },
        "scale-y":{
            "background-color":"#fff",
            "border-width":"1px",
            "border-color":"#333",
            "alpha":0.5,
            "format":"$%v",
            "guide":{
                "line-style":"solid",
                "line-color":"#333",
                "alpha":0.2
            },
            "tick":{
                "line-color":"#333",
                "alpha":0.2
            },
            "item":{
                "font-color":"#333",
                "padding-right":"6px"
            }
        },
        "scale-x":{
            "background-color":"#fff",
            "border-width":"1px",
            "border-color":"#333",
            "alpha":0.5,
            "values":["Fixed acidity","Volatile acidity", "Citric acid","Residual sugar", "Chlorides","Free sulfur dioxide", "Total sulfur dioxide","Density", "pH","Sulphates", "Alcohol"],
            "guide":{
                "visible":false
            },
            "tick":{
                "line-color":"#333",
                "alpha":0.2
            },
            "item":{
                "font-size":"11px",
                "font-color":"#333"
            }
        },
        "series":[
            {
                "values":[
         Number($("#fixedAcidity").val())-60, Number($("#volatileAcidity").val())-64,
        Number($("#citricAcid").val())-21,Number($("#residualSugar").val())-99,Number($("#chlorides").val())-60,
        Number($("#freeSulfurDioxide").val())-70,Number( $("#totalSulfurDioxide").val())-67,Number( $("#density").val())-60,
       Number($("#pH").val())-32, Number($("#sulphates").val())-80, Number($("#alcohol").val())-49
    ],
                "text":"Diference",
                "background-color":"#03A9F4 #4FC3F7",
                "border-color":"#03A9F4",
                "legend-marker":{
                    "border-color":"#03A9F4"
                },
                "tooltip":{
                    "background-color":"#03A9F4",
                    "text":"$%v",
                    "font-size":"12px",
                    "padding":"6 12",
                    "border-color":"none",
                    "shadow":0,
                    "border-radius":5
                }
            }
        ]
    };
     
    zingchart.render({ 
    	id : 'myChart8', 
    	data : myConfig2, 
    	height: 500, 
    	width: 725,
    	defaults:{
    	  'font-family':'sans-serif'
    	}
    });


});



    

