//Priprava knjižnic
var formidable = require("formidable");


if (!process.env.PORT)
  process.env.PORT = 8080;

// Priprava strežnika
var express = require('express');
var streznik = express();
streznik.set('view engine', 'ejs');
streznik.use(express.static('public'));
streznik.use(express.json({"type": "*/*"}));

// Prikaz seznama pesmi na strani
streznik.get('/', function(zahteva, odgovor) {
  odgovor.render('index', {});
});

//Obdelava vhodnih podatkov
streznik.post('/postData', function(zahteva, odgovor) {
  //express.json();
  console.log(zahteva.body);
});

streznik.listen(process.env.PORT, function() {
  console.log("Strežnik pognan!");
});