from keras.models import Sequential
from keras.layers import Dense
from keras.models import model_from_json
import numpy as np
import csv
import os
import sys
def pridobiPodatke(type):
    import json

    if (type == 1):
        f = open('winequality/winequality-white.csv', 'rt')
    else :
        f = open('winequality/winequality-red.csv', 'rt')

    data = []
    try:
        reader = csv.reader(f)
        next(reader)
        for row in reader:
            row = row[0].split(';')
            for i in range(len(row)):
                row[i] = float(row[i])
            data.append(row)
    finally:
        f.close()
    data = json.dumps({'results': data})
    print(data)
    exit(0)
    
pridobiPodatke(sys.argv[1])