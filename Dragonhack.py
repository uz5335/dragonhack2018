'''Trains a simple convnet on the MNIST dataset.
Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''


import DataIn

import keras
from keras.datasets import mnist
from keras.models import Sequential
import numpy as np
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
import tensorflow as tf
with tf.device('/cpu:0'):
    batch_size = 10
    num_classes = 10
    epochs = 6

    # the data, split between train and test sets
    x_train, y_train, x_test,y_test = DataIn.funkcija()
  #  x_train, x_test, y_train, y_test = DataIn.funkcija()


    x_test = np.array(x_test)
    x_train=np.array(x_train)
    y_train=np.array(y_train)
    y_test=np.array(y_test)

    print("AUbdaifhawihdo", x_train[0])
    #x_train = x_train / x_train.max(axis=0)
    #x_test = x_test / x_test.max(axis=0)

    '''x_train = tf.keras.utils.normalize(
       x_train,
        axis=0,
        order=2
    )
    print("AUbdaifhawihdo", x_train[0])'''

    x_train = np.asarray(x_train)
    x_test = np.asarray(x_test)

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')

    # print(x_test, x_train)
    print('x_train shape:', x_train.shape)
    print(x_train.shape[0], 'train samples')
    print(x_test.shape[0], 'test samples')

    model = Sequential()
    model.add(Dense(20, input_shape=(12,), activation='sigmoid'))
    model.add(Dense(20, activation='sigmoid'))
    model.add(Dense(10, activation='sigmoid'))

    print("naredil")

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])
    print("x_data", x_train[0])
    print("y_train", y_train[0])

    print(x_train[0], y_train[0])
    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=1,
              validation_data=(x_test, y_test))
    score = model.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    a =[]
    testdata = np.array([[0,6.2,0.35,0.03,1.2,0.064,29,120,0.9934,3.22,0.54,9.1]])

    model_json = model.to_json()
    with open("model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("model.h5")
    print("Saved model to disk")

    rez = model.predict_classes(testdata)
    print(rez)