import csv
import random
import sys

def funkcija():
    f = open('winequality/winequality-white.csv', 'rt')
    g = open('winequality/winequality-red.csv', 'rt')
    data = []
    try:
        reader = csv.reader(f)
        next(reader)
        for row in reader:
            row = row[0].split(';')
            for i in range(len(row)):
                row[i] = float(row[i])
            row.insert(0,0)
            data.append(row)
    finally:
        f.close()

    try:
        reader = csv.reader(g)
        next(reader)
        for row in reader:
            row = row[0].split(';')
            for i in range(len(row)):
                row[i] = float(row[i])
            row.insert(0,1)
            data.append(row)
    finally:
        g.close()
    random.shuffle(data)
    print(len(data))
    train = data[:5900]
    test = data[5900:]

    x_train = []
    x_test = []

    y_train = []
    y_test = []

    for i in train:
        x_train.append(i[:12])
        stevilo = int(i[12])
        neki = [0,0,0,0,0,0,0,0,0,0]
        neki[stevilo] = 1
        y_train.append(neki)
    for i in test:
        x_test.append(i[:12])
        stevilo = int(i[12])
        neki = [0,0,0,0,0,0,0,0,0,0]
        neki[stevilo] = 1
        y_test.append(neki)


    f.close()
    g.close()
    print(x_train[0], y_train[0])
    return(x_train,y_train, x_test, y_test)

funkcija()
