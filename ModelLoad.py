from keras.models import Sequential
from keras.layers import Dense
from keras.models import model_from_json
import numpy as np
import csv
import os

testdata = np.array([[0, 6.2, 0.35, 0.03, 1.2, 0.064, 29, 120, 0.9934, 3.22, 0.54, 9.1]])


data1 = np.array([[0, 6.7,0.24,0.39,2.9,0.173,63,157,0.9937,3.1,0.34,9.4]])
data5 = np.array([[1, 6.7,23,0.33,2.9,0.173,63,157,0.9937,3.1,0.34,9.4]])
data6 = np.array([[1, 6.7,0.24,0.33,2.9,45,63,157,0.9937,3.1,0.34,9.4]])
data7 = np.array([[0, 6.7,0.24,33,2.9,0.173,63,157,0.9937,3.1,0.34,9.4]])

data2 = np.array([[1,1,1,22,8,1,1,4,1,1,1,1]])
data3 = np.array([[0,0,0,22,8,99999,0,4,0,0,0,0]])
data4 = np.array([[0,6.7,0.23,0.26,1.4,0.06,33,154,0.9934,3.24,0.56,9.5]])


utezi  =[ 1,   15.9  ,     1.58     , 1.66   ,  65.8     ,  0.611   ,289,
 440.    ,    1.03898  , 4.01     , 2.    ,   14.9    ]
for i in range (12):
   ''' data1[0][i] /= utezi[i]
    data2[0][i] /= utezi[i]
    data3[0][i] /= utezi[i]
    data4[0][i] /= utezi[i]
    data5[0][i] /= utezi[i]
    data6[0][i] /= utezi[i]
    data7[0][i] /= utezi[i]
'''





json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("model.h5")
print("Loaded model from disk")
print(data1)
print(data2)
print(data3)
print(data4)
print(data5)
print(data6)

rez = loaded_model.predict_classes(data2)
print(rez)
rez = loaded_model.predict_classes(data3)
print(rez)
rez = loaded_model.predict_classes(data4)
print(rez)
rez = loaded_model.predict_classes(data1)
print(rez)

rez = loaded_model.predict_classes(data5)
print(rez)
rez = loaded_model.predict_classes(data6)
print(rez)
rez = loaded_model.predict_classes(data7)
print(rez)
#for layer in loaded_model.layers:
   # print(layer.get_weights())

def napovej(q,w,e,r,t,z,u,i,o,p,a,s):
    set = [q,w,e,r,t,z,u,i,o,p,a,s]
    data = np.array([set])
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("model.h5")
    print("Loaded model from disk")

    rez = loaded_model.predict_classes(data)[0]
    toprez =0
    topplus = 0
    topoperand = ""
    topatribut = ""

    print(rez)
    for procenti in range (100):
        print("PROCENTI = ", procenti)
        if(toprez >rez):
            return (rez, toprez, topplus, topoperand, topatribut)
        for atribut in range (12):
            print("ATRIBUTI = ", atribut)
            set2 = set
            set2[atribut] = set2[atribut]+ set2[atribut] * procenti/100
            rez2 = loaded_model.predict_classes(np.array([set2]))[0]
            print("     ", rez2)
            if (rez2 > rez):
                if (rez2 > toprez):
                    toprez = rez2
                    topplus = procenti
                    topoperand = 1
                    topatribut = atribut
            set2 = set
            set2[atribut] = set2[atribut] - procenti / 10
            rez2 = loaded_model.predict_classes(np.array([set2]))[0]
            print("     ", rez2)
            if (rez2 > rez):
                if (rez2 > toprez):
                    toprez = rez2
                    topplus = procenti
                    topoperand = 0
                    topatribut = atribut





def pridobipodatke(type):
    import json

    if (type == 1):
        f = open('winequality/winequality-white.csv', 'rt')
    else :
        f = open('winequality/winequality-red.csv', 'rt')

    data = []
    try:
        reader = csv.reader(f)
        next(reader)
        for row in reader:
            row = row[0].split(';')
            for i in range(len(row)):
                row[i] = float(row[i])
            data.append(row)
    finally:
        f.close()
    data = json.dumps({'results': data})
    print(data)
    return data

